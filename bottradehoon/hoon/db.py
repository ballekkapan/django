from os import name
from pprint import pprint
from asgiref.sync import sync_to_async, async_to_sync
from .models import Condition, Realtime, Result
from datetime import datetime
import logging
from contextlib import suppress

stock_data = {
    'data': '',
    'event': '',
}

# Create instance
logger = logging.getLogger('hoon')

# **********************************************************
# *  Description: Load data Conditions from database       *
# *                                                        *
# *  Date:          Version:    Programmer:       Reason:  *
# *  2021-10-14      1.0        Ekkapan C.        created  *
# **********************************************************


def load_condition(symbol):
    queryset = Condition.objects.get(symbol=symbol)

    qty = queryset.quantity
    # Json
    data_cover = queryset.cover_conf
    list_percent = data_cover['percent'][:qty]
    list_multiply = data_cover['multiply'][:qty]
    list_drop_pullback = data_cover['drop_pullback'][:qty]

    return {
        'amount': queryset.amount,
        'quantity': qty,
        'percent_drop': list_percent,
        'multiply': list_multiply,
        'percent_profit': queryset.percent_profit,
        'percent_retracement': queryset.percent_retracement,
        'drop_pullback': list_drop_pullback,
        'stop_loss': queryset.stop_loss,
    }


# **********************************************************
# *  Description: Load data Conditions from database       *
# *                                                        *
# *  Date:          Version:    Programmer:       Reason:  *
# *  2021-10-31      1.0        Ekkapan C.        created  *
# **********************************************************
def load_reltimes(symbol):
    queryset = Realtime.objects.get(name=symbol)

    return {
        'symbol': queryset.symbol,
        'times': queryset.times,
        'init_price': queryset.init_price,
        'high_price': queryset.high_price,
        'low_price': queryset.low_price,
        'cost_total': queryset.cost_total,
        'stock_total': queryset.stock_total,
        'status_order': queryset.status_order,
        'last_order_no': queryset.last_order_no,
        'condition': queryset.condition
    }


# **********************************************************
# *  Description: Save result data to database             *
# *                                                        *
# *  Date:          Version:    Programmer:       Reason:  *
# *  2021-10-31      1.0        Ekkapan C.        created  *
# **********************************************************
def result_reltimes(name, time, side, data, port, realtime):

    del data['condition']
    symbol = data['symbol']
    realize_profit = port['realize_profit']
    json_data = {'condition': data, 'portfolio': port, 'realtime': realtime}

    result = Result(name=name,
                    time=time,
                    symbol=symbol,
                    realize_profit=realize_profit,
                    side=side,
                    json_data=json_data)
    result.save()


# **********************************************************
# *  Description: Function update condition for the        *
# *               Auto Trade                               *
# *  Date:          Version:    Programmer:       Reason:  *
# *  2021-10-14      1.0        Ekkapan C.        created  *
# **********************************************************
@ sync_to_async
def init_realtime_trade(symbol, json_last_price, times):
    try:
        last_price = json_last_price[symbol]
    except:
        last_price = 0.0

    condition = Condition.objects.get(symbol=symbol)
    realtime = Realtime(name=symbol, symbol=symbol,
                        condition=condition, init_price=last_price, high_price=last_price, low_price=last_price, times=times, status_order='M')
    realtime.save()

# **********************************************************
# *  Description: Check Database is exits?                 *
# *               Auto Trade                               *
# *  Date:          Version:    Programmer:       Reason:  *
# *  2021-11-17      1.0        Ekkapan C.        created  *
# **********************************************************


@ sync_to_async
def filter_database(symbol):
    realtime = Realtime.objects.filter(name=symbol).count()
    return realtime


@ sync_to_async
def set_trade_status(symbol, state):
    realtimes = Realtime.objects.get(name=symbol)
    realtimes.trading_status = state
    realtimes.save(update_fields=['trading_status'])


@ sync_to_async
def clear_status(symbol):
    realtimes = Realtime.objects.get(name=symbol)
    realtimes.status_order = 'M'
    realtimes.save(update_fields=['status_order'])

# **********************************************************
# *  Description: Update data place order to realtime      *
# *               & result database                        *
# *                                                        *
# *  Date:          Version:    Programmer:       Reason:  *
# *  2021-10-14      1.0        Ekkapan C.        created  *
# **********************************************************


def update_database_order(result, port, realtime):
    side = result['side']
    volume = result['volume']
    order_no = result['order_no']
    symbol = result['symbol']

    realtimes = Realtime.objects.get(name=symbol)
    if side == 'BUY':
        realtimes.times += 1
    realtimes.status_order = 'M'
    realtimes.save(update_fields=['times', 'status_order'])
    data = load_reltimes(symbol)

    # Stop Subscript
    try:
        amount = port['amount']
        actual_volume = port['actual_volume']
        last_price = port['market_price']
        # Save log database
        qty = data['times']
        data['stock_total'] = volume
        data['cost_total'] = amount
    except:
        amount = 0
        actual_volume = 0
        last_price = 0
        # Save log database
        qty = data['times']
        data['stock_total'] = volume
        data['cost_total'] = amount

    logger.debug("side = %s, average_price = %.2f", side, last_price)

    now = datetime.now()
    if side == 'BUY':
        name = f'{now}'
    else:
        name = f'{now}'

    if side == 'BUY':
        # Update Realtimes
        # semaphore.acquire()
        realtimes.status_order = 'M'
        realtimes.init_price = last_price
        realtimes.low_price = last_price
        realtimes.high_price = last_price
        realtimes.stock_total = actual_volume
        realtimes.cost_total = amount
        realtimes.last_order_no = order_no
        realtimes.save()
        # semaphore.release()

    data['last_order_no'] = order_no
    result_reltimes(name, qty, side, data, port, realtime)
    logger.debug("Update data to realtimes & result already")
    return side
