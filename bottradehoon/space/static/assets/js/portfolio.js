// Initial
let unrealized = 0;
let percent_unrealized = 0;
let realized = 0;
let total_amount = 0;
const table = document.getElementById("table-portfolio");
let id_total = ["port.unrealized", "port.percent_unrealized", "port.realized"];
getPortfolio();

// Event onclick portfolio button
document.getElementById("btn-port").onclick = function () {
  getPortfolio().then((response) => {
    table.innerHTML = "";
    unrealized = 0;
    percent_unrealized = 0;
    realized = 0;
    total_amount = 0;
    console.log("Portfolio", response);
    response.forEach(updatePortfolio);
    // percent_unrealized = Unrealized * 100 / Amount
    percent_unrealized = (unrealized * 100) / total_amount;
    id_total.forEach(totalProfit);
  });
};

// get portfolio
function getPortfolio() {
  let url = "/api/portfolio";
  return fetch(url)
    .then((res) => res.json())
    .then((data) => {
      // Clear table
      return data;
    });
}

function totalProfit(id_name, index) {
  let num_total = [unrealized, percent_unrealized, realized];
  id = document.getElementById(id_name);

  if (id_name == "port.percent_unrealized") {
    id.innerHTML = `(${num_total[index].toLocaleString("en-US")}%)`;
  } else {
    id.innerHTML = num_total[index].toLocaleString("en-US");
  }

  if (num_total[index] > 0) {
    id.className = "color-success";
  } else if (num_total[index] < 0) {
    id.className = "color-danger";
  } else {
  }
}

//**********************************************************
//*  Description: Update History Table                     *
//*                                                        *
//*  Date:          Version:    Programmer:       Reason:  *
//*  2021-10-04      1.0        Ekkapan C.        created  *
//**********************************************************
function updatePortfolio(data) {
  // Get Tabel
  const col_num = 8;

  // Unrealized P/L Mkt Value - Amount
  // Total Calculate
  realized += data["realize_profit"];
  unrealized += data["profit"];
  total_amount += data["amount"];

  port_data = [
    data["symbol"],
    data["actual_volume"],
    data["market_price"],
    data["amount"],
    data["market_value"],
    data["profit"],
    data["percent_profit"],
    data["realize_profit"],
  ];

  // Insert Row
  let newRow = table.insertRow(0);

  // Insert Column
  for (let i = 0; i < col_num; i++) {
    // add cell
    let newCell = newRow.insertCell(i);
    let newText = document.createTextNode(port_data[i].toLocaleString("en-US"));
    if (i == 0) {
      newCell.style.left = "0px";
      newCell.className = "sticky-left";
    } else if (i == 1) {
      newCell.className = "color-warning";
    } else if (i == 5 || i == 6) {
      number = port_data[i];
      if (number > 0) {
        newCell.className = "color-success";
      } else if (number < 0) {
        newCell.className = "color-danger";
      }
    }
    newCell.appendChild(newText);
  }
}
