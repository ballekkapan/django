from django.contrib import admin
from django.contrib.admin.decorators import register
from django.contrib.admin.sites import AdminSite
from django.db import models
from django_json_widget.widgets import JSONEditorWidget
from .models import Condition, Realtime, Result

# Register your models here.


@admin.register(Condition)
class ModelAdmin(admin.ModelAdmin):
    list_display = ['symbol', 'name', 'percent_profit',
                    'stop_loss']
    formfield_overrides = {
        models.JSONField: {'widget': JSONEditorWidget},
    }


@admin.register(Result)
class ModelAdmin(admin.ModelAdmin):
    list_display = ['name', 'created_at', 'time', 'side',
                    'symbol', 'realize_profit']
    list_per_page = 10
    # list_editable = ['profit','percent_profit']
    list_filter = ['symbol', 'created_at', 'side']
    search_fields = ['name']
    date_hierarchy = 'created_at'
    # fields = ['symbol', 'name']
    formfield_overrides = {
        models.JSONField: {'widget': JSONEditorWidget},
    }


admin.site.register(Realtime)

# https://github.com/django/django/tree/main/django/contrib/admin/templates/admin
