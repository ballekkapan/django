from django.apps import AppConfig


class HoonConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'hoon'
