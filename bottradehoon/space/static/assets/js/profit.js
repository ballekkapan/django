// Global Variable
const table_monthly = document.getElementById("table-monthly");
const table_daily = document.getElementById("table-daily");
let paginator;
let daily_data = {};
let monthly_data = {};

document.getElementById("btn-profit").onclick = function () {
  console.log("profit");
  getProfitMonthly(1);
  getProfitDaily(1);
};

document.getElementById("monthly-previous").onclick = function (e) {
  id = document.getElementById("monthly-page");
  page = id.childNodes[0].innerHTML;
  page = (parseInt(page) + (paginator - 1)) / paginator;
  getProfitMonthly(page - 1);
};

document.getElementById("monthly-next").onclick = function (e) {
  id = document.getElementById("monthly-page");
  page = id.childNodes[0].innerHTML;
  page = (parseInt(page) + (paginator - 1)) / paginator;
  getProfitMonthly(page + 1);
};

document.getElementById("daily-previous").onclick = function (e) {
  id = document.getElementById("daily-page");
  page = id.childNodes[0].innerHTML;
  page = (parseInt(page) + (paginator - 1)) / paginator;
  getProfitDaily(page - 1);
};
document.getElementById("daily-next").onclick = function (e) {
  id = document.getElementById("daily-page");
  page = id.childNodes[0].innerHTML;
  page = (parseInt(page) + (paginator - 1)) / paginator;
  getProfitDaily(page + 1);
};

// get profit
function getProfitMonthly(page) {
  let url = `/api/get_profit/?page=${page}`;
  fetch(url)
    .then((res) => res.json())
    .then((data) => {
      table_monthly.innerHTML = "";
      let monthly = data["monthly"];
      paginator = data["per_page"];
      updateMonthly(monthly, page);
      createTotal(data["total"]);
    });
}

// get profit
function getProfitDaily(page) {
  let url = `/api/get_profit/?page=${page}`;
  fetch(url)
    .then((res) => res.json())
    .then((data) => {
      table_daily.innerHTML = "";
      let daily = data["daily"];
      paginator = data["per_page"];
      updateDaily(daily, page);
      createTotal(data["total"]);
    });
}

//**********************************************************
//*  Description: Update History Table                     *
//*                                                        *
//*  Date:          Version:    Programmer:       Reason:  *
//*  2021-10-04      1.0        Ekkapan C.        created  *
//**********************************************************
function updateMonthly(monthly, page) {
  // Get Tabel
  const current_page = document.getElementById("monthly-page");
  const previous = document.getElementById("monthly-previous");
  const next = document.getElementById("monthly-next");

  createTable(monthly, page, table_monthly, current_page, previous, next);
}

//**********************************************************
//*  Description: Update History Table                     *
//*                                                        *
//*  Date:          Version:    Programmer:       Reason:  *
//*  2021-10-04      1.0        Ekkapan C.        created  *
//**********************************************************
function updateDaily(data_profit, page) {
  // Get Tabel
  const current_page = document.getElementById("daily-page");
  const previous = document.getElementById("daily-previous");
  const next = document.getElementById("daily-next");

  createTable(data_profit, page, table_daily, current_page, previous, next);
}

//**********************************************************
//*  Description: Create Table Profit                      *
//**********************************************************
function createTable(data_profit, page, table, current_page, previous, next) {
  const col_num = 4;

  data = data_profit["data"];
  count = data_profit["count"];
  num_pages = data_profit["num_pages"];
  page_action = page;

  if (num_pages == 1) {
    previous.disabled = true;
    previous.classList.add("disabled");
    next.disabled = true;
    next.classList.add("disabled");
  } else if (page <= 1) {
    // first page
    previous.disabled = true;
    previous.classList.add("disabled");
    next.disabled = false;
    next.classList.remove("disabled");
  } else if (page >= num_pages) {
    // last page
    previous.disabled = false;
    previous.classList.remove("disabled");
    next.disabled = true;
    next.classList.add("disabled");
  }

  let page_start = page_action * paginator - (paginator - 1);
  let page_end = page_action * paginator;
  if (page_end >= count) {
    page_end = count;
  }
  if (page_start == page_end) {
    current_page.innerHTML = `<span>${page_start}</span> of ${count}`;
  } else {
    current_page.innerHTML = `<span>${page_start}</span> - ${page_end} of ${count}`;
  }

  for (let k in data) {
    // Insert Row
    let newRow = table.insertRow(0);
    // Insert Column
    for (let i = 0; i < col_num; i++) {
      // add cell
      let newCell = newRow.insertCell(i);
      let newText = document.createTextNode(data[k].toLocaleString("en-US"));
      if (i == 0) {
        // First Column
        newText = document.createTextNode(k);
        newCell.style.left = "0px";
        newCell.className = "sticky-left";
      } else if (i == 1) {
        // Profit
        newCell.className = "color-success";
        newText = document.createTextNode(data[k][0].toLocaleString("en-US"));
      } else if (i == 2) {
        // Profit
        newCell.className = "color-danger";
        newText = document.createTextNode(data[k][1].toLocaleString("en-US"));
      } else {
        let nett = data[k][0] + data[k][1];
        newText = document.createTextNode(nett.toLocaleString("en-US"));
      }
      newCell.appendChild(newText);
    }
  }
}

function createTotal(total) {
  let profit = total["profit"];
  let loss = total["loss"];
  let nett = total["nett"];

  document.getElementById("total-profit").innerHTML =
    profit.toLocaleString("en-US");
  document.getElementById("total-loss").innerHTML =
    loss.toLocaleString("en-US");
  document.getElementById("total-nett").innerHTML =
    nett.toLocaleString("en-US");
}
