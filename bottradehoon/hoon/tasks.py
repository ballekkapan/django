import threading
from typing import cast
from celery import shared_task
from threading import Semaphore, Thread
from .db import load_reltimes
from decouple import config
import re
import time
from pprint import pprint, pformat
from .models import Realtime
import requests
import datetime
from bs4 import BeautifulSoup
import math
from .settrade_api import trade, send_data_to_group, current_realtime_trade, current_portfolio
from contextlib import suppress
import logging
import asyncio

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0'}
# https://testdriven.io/courses/django-celery/auto-reload/

# Global Variable
current_symbol = ''
semaphore = Semaphore(1)
data_market = {}
interval_time = config('TIME_INTERVAL_PRICE', cast=float)
interval_trade = config('TIME_INTERVAL_TRADE', cast=float)
show_realtime_price = config('SHOW_REALTIME_PRICE', cast=bool)
show_trade_data = config('SHOW_TRADE_DATA', cast=bool)
is_realtime_api = config('REALTIME_PRICE_FROM_API', cast=bool)
precision = 2
time_open_i = {'start': datetime.time(10, 0), 'end': datetime.time(12, 30)}
time_open_ii = {'start': datetime.time(14, 30), 'end': datetime.time(16, 30)}

# Crate instance trade api
trade_api = trade()
logger = logging.getLogger('hoon')
session_request = requests.Session()

# **********************************************************
# *  Description: Realtime Price From Celery               *
# *                                                        *
# *  Date:          Version:    Programmer:       Reason:  *
# *  2021-10-15      1.0        Ekkapan C.        created  *
# **********************************************************


@shared_task(bind=True)
def realtime_equity(self, symbol):
    # Object Data
    stock_data = {
        'data': '',
        'event': '',
    }
    ticker = symbol
    status, data = scrapingSettrade(ticker)

    if status:
        # Send data to group
        stock_data['event'] = 'realtime_stock'
        stock_data['data'] = data
        send_data_to_group(stock_data, symbol)


# **********************************************************
# *  Description: Web Scraping from Settrade               *
# *                                                        *
# *  Date:          Version:    Programmer:       Reason:  *
# *  2021-10-15      1.0        Ekkapan C.        created  *
# **********************************************************
def scrapingSettrade(symbol):
    result = {}
    price_list = []
    status_list = []
    global data_market
    global session_request

    url = f'https://www.settrade.com/C04_01_stock_quote_p1.jsp?txtSymbol={symbol}&ssoPageId=9&selectPage=1'

    try:
        response = session_request.get(url)
    except Exception as e:
        logging.warning(e)
        session_request = requests.Session()

    response.encoding = 'utf-8'
    logger.debug("requests scraping data done")

    if response.status_code == 200:
        soup = BeautifulSoup(response.text, 'html.parser')
        # print(soup.prettify())
        prices = soup.find_all('h1')

        # print(prices)
        for data in prices[1:]:  # price | change | percent
            price = data.text
            price = re.search(r'[-0-9]+.[-0-9]+', price.strip())
            try:
                price_list.append(price.group(0))
            except:
                price_list.append(0.0)

        price_len = len(price_list)
        # print(price_list)

        market = soup.find_all('span')

        for index, data in enumerate(market):
            status = data.text.strip()
            status_list.append(status)
            if status == symbol:
                index_sym = index
                break

        status = status_list[-3:-1]

        result['symbol'] = symbol
        if price_len == 3:
            result['last'] = float(price_list[0])
            result['change'] = float(price_list[1])
            result['percent'] = float(price_list[2])
        else:
            result['last'] = 0
            result['change'] = 0
            result['percent'] = 0

        if market:
            result['time'] = status[0]
            result['status'] = status[1]
        else:
            session_request = requests.Session()
            logger.debug("Restart Requests Session")

        data_market[symbol] = result
        return True, result

    else:
        return False, result


# **********************************************************
# *  Description: Realtime Price Thread                    *
# *                                                        *
# *  Date:          Version:    Programmer:       Reason:  *
# *  2021-10-15      1.0        Ekkapan C.        created  *
# **********************************************************
class realtimePrice(Thread):

    def __init__(self, symbol):
        Thread.__init__(self)
        self.previous_price = 0
        self.ticket = symbol
        self.daemon = True
        self.__stop_event = threading.Event()

    def stop(self):
        self.__stop_event.set()

    def stutus(self):
        return self.status

    def run(self):
        global current_symbol
        stock_data = {
            'data': '',
            'event': '',
        }
        while not self.__stop_event.is_set():
            semaphore.acquire()
            if self.ticket != '':
                stock_data['event'] = 'realtime_stock'
                port = trade_api.portfolio(self.ticket)
                status_port = port['status']

                if status_port and is_realtime_api:
                    str_time = datetime.datetime.now().strftime("P | %Y/%m/%d %H:%M:%S")
                    last_price = port['market_price']

                    if self.previous_price == 0:
                        self.previous_price = last_price

                    percent, change = calculate_profit(
                        last_price, self.previous_price)
                    status = check_market_status()

                    market = {'change': change,
                              'last': last_price,
                              'percent': percent,
                              'status': status,
                              'symbol': self.ticket,
                              'time': str_time}

                    self.previous_price = last_price
                    stock_data['data'] = market
                    data_market[self.ticket] = market
                    send_data_to_group(stock_data, self.ticket)

                else:
                    # Object Data
                    status, data = scrapingSettrade(self.ticket)
                    if status:
                        # Send data to group
                        stock_data['data'] = data
                        self.previous_price = data['last']
                        send_data_to_group(stock_data, self.ticket)

                if show_realtime_price:
                    print()
                    logger.info('\n' + pformat(stock_data, indent=2))

            semaphore.release()
            self.status = True
            time.sleep(interval_time)


# **********************************************************
# *  Description: Realtime Auto-Trade Thread               *
# *                                                        *
# *  Date:          Version:    Programmer:       Reason:  *
# *  2021-10-15      1.0        Ekkapan C.        created  *
# **********************************************************
class realtimeAutoTrade(Thread):

    def __init__(self, symbol):
        Thread.__init__(self)
        self.symbol = symbol
        self.daemon = True
        self.status = ['Open(I)', 'Open(II)', 'Portfolio']  # Can place order
        self.status_close = ['Intermission',
                             'Pre-Open2', 'OffHour', 'Closed', 'Pre-close', 'Pre-Open1']  # Can't place order
        self.__stop_event = threading.Event()

    def stop(self):
        self.__stop_event.set()

    def run(self):
        while not self.__stop_event.is_set():
            # Critical Section, Read Realtimes
            semaphore.acquire()
            data = load_reltimes(self.symbol)
            condition = data['condition']
            qty = condition.quantity
            stop_loss = condition.stop_loss
            times = data['times']

            # Check status
            status = data_market[self.symbol]['status'] in self.status
            last_price = data_market[self.symbol]['last']
            cost = data['cost_total']

            # Get Portfolio
            port = trade_api.portfolio(self.symbol)
            stock_data = {}
            buy_data = {}

            profit_check, sell_data = profit_check_sell(
                data, last_price, port)

            # Profit for sell
            if (profit_check and port['actual_volume'] > 0):
                # Send data to Sell
                retracement = sell_stock(data, status, last_price, port, False)
                sell_data['retracement'] = retracement
                status_msg = 'หุ้นเพิ่มขึ้นเตรียมขายเมื่อหุ้นลดลง'

            # Check Stop Loss
            elif (port['percent_profit'] < stop_loss):
                # Force Sell
                retracement = sell_stock(data, status, last_price, port, True)
                sell_data['retracement'] = retracement
                status_msg = 'ทำการ Stop Loss'

            # Waiting For Sell
            elif times > qty:
                logger.debug('Waiting for sell')
                send_data_to_group(
                    {'event': 'disable', 'data': '#button-buy'}, self.symbol)
                status_msg = 'ช้อนซื้อครบแล้ว รอกำไรเพื่อขาย'

            # Buy
            else:
                # Send data to Buy
                send_data_to_group(
                    {'event': 'enable', 'data': '#button-buy'}, self.symbol)
                buy_data, status_msg = check_profit_buy(
                    data, status, last_price, self.symbol)

            # Check Market Status
            if not status:
                status_msg = data_market[self.symbol]['status']
            # Send data to group
            stock_data['event'] = 'realtime_profit'
            stock_data['data'] = {'SELL': sell_data,
                                  'BUY': buy_data, 'group_name': self.symbol, 'status': status_msg}

            curr = current_realtime_trade(stock_data)
            if show_trade_data:
                print()
                logger.info('\n' + pformat(curr, indent=2))
            send_data_to_group(curr, self.symbol)
            semaphore.release()
            time.sleep(interval_trade)
        logger.debug('stopped!')

# **********************************************************
# *  Description: (C) Check profit                         *
# *                                                        *
# *  Date:          Version:    Programmer:       Reason:  *
# *  2021-10-31      1.0        Ekkapan C.        created  *
# **********************************************************


def profit_check_sell(data, last_price, port):
    cost_total = data['cost_total']  # Amount
    stock_total = data['stock_total']
    condition = data['condition']
    market_value = stock_total * last_price
    BOARD_LOT = 100
    port_status = port['status']

    profit_total = (last_price * stock_total) - cost_total
    # percent_profit, profit = calculate_profit(cost_total, market_value)
    if port_status:
        volume = port['actual_volume']
        volume -= volume % BOARD_LOT
        percent_profit = port['percent_profit']
        profit = port['profit']
        volume = port['actual_volume']
    else:
        percent_profit = 0
        profit = 0
        volume = 0
    data = {'%profit': round_down(percent_profit, precision),
            'profit': round_down(profit, precision),
            'profit_total': round_down(profit, precision),
            'volume': volume,
            'retracement': 0.0,
            'last_price': last_price,
            'init_price': data['init_price'],
            'high_price': data['high_price']}

    if (percent_profit >= condition.percent_profit):
        return True, data
    return False, data


# **********************************************************
# *  Description: Buy                                      *
# *                                                        *
# *  Date:          Version:    Programmer:       Reason:  *
# *  2021-10-31      1.0        Ekkapan C.        created  *
# **********************************************************
def check_profit_buy(data, status, last_price, symbol):
    # Case (A)
    BOARD_LOT = 100
    condition = data['condition']
    init_price = data['init_price']
    current_times = data['times']
    times = data['times'] - 1
    json_cover = condition.cover_conf
    try:
        percent = json_cover['percent'][times]
        drop_pullback = json_cover['drop_pullback'][times]
        multiply = json_cover['multiply'][times]
    except IndexError as e:
        logger.debug('{%d} %s', current_times, e)
        return {'times': current_times}, f'({current_times}) ครั้งที่ช้อนซื้อไม่ถูกต้องโปรดเช็ค Cover Conditions'

    # Calculate amount equity
    amount = condition.amount
    amount = amount*multiply

    try:
        volume = math.floor(amount / last_price)
        volume -= volume % BOARD_LOT
    except:
        volume = 0

    # เปอร์เซ็นต์กําไร = กำไร / ยอดขาย คูณด้วย 100% และ กำไร = ยอดขายรวม – ต้นทุนรวม
    percent_profit, profit = calculate_profit(last_price, init_price)
    percent_profit = round_down(percent_profit, precision)
    profit = round_down(profit, precision)
    buy_data = {'times': times + 1,
                '%profit': percent_profit,
                'multiply': multiply,
                'profit': profit,
                'percent_buy': percent,
                'volume': volume,
                'last_price': last_price,
                'drop_pullback': drop_pullback,
                '%drop_pullback': 0,
                'estimate_amount': round_down(volume * last_price, 2),
                'low_price': data['low_price']
                }

    qty = data['times'] - 1 < condition.quantity  # Check qty for buy

    if status and qty:
        if (percent_profit >= percent):  # Check profit from cover configure
            # Calucate low price
            if last_price < data['low_price']:
                low_price = last_price
                logger.debug('Update low price = %.2f฿, Symbol = %s',
                             low_price, symbol)
                # semaphore.acquire()
                realtime = Realtime.objects.get(name=symbol)
                # semaphore.release()
                realtime.low_price = low_price
                realtime.save(update_fields=['low_price'])
            else:
                low_price = data['low_price']

            percent_pullback, profit = calculate_profit(low_price, last_price)
            buy_data['%drop_pullback'] = percent_pullback
            logger.info('\n' + pformat(buy_data, indent=2))
            # Case A
            # Check drop pullback
            # and data['status_order'] == 'M'):
            if (percent_pullback >= drop_pullback and data['status_order'] == 'M'):
                logger.debug("Buy issue..")
                # semaphore.acquire()
                realtime = Realtime.objects.get(name=symbol)
                # semaphore.release()
                realtime.status_order = 'Pending(S)'
                realtime.save(update_fields=['status_order'])

                # Send data to group
                stock_data = {}
                stock_data['event'] = 'place_order_buy'

                # Calculate amount equity
                amount = condition.amount
                amount = amount*multiply

                with suppress(ZeroDivisionError):
                    volume = math.floor(amount / last_price)
                    volume -= volume % BOARD_LOT

                # BUY
                current_portfolio(symbol)
                send_data_to_group(
                    {'event': 'disable', 'data': '#button-buy'}, symbol)
                # trade_api.equity_place_order('BUY', symbol, volume)
                loop = asyncio.new_event_loop()
                asyncio.set_event_loop(loop)
                loop.run_until_complete(place_oder('BUY', symbol, volume))

                return buy_data, f'ทำการซื้อหุ้นครั้งที่ {current_times}'
            return buy_data,  f'ช้อนซื้อครั้งที่ {current_times} รอราคาหุ้นดีดกลับ'
        else:
            logger.debug("Not buy, profit < criteria")
            return buy_data, f'ช้อนซื้อครั้งที่ {current_times} รอราคาหุ้นลดลง'

    else:
        logger.debug(
            'Cannot place Market Price order when market is not open or quantity incorrect')
        return buy_data, "ตลาดปิดไม่สามารถทำการซื้อขายได้"


async def place_oder(side, symbol, volume):
    return await trade_api.equity_place_order(side, symbol, volume)


def calculate_profit(cost_total, profit_total):
    # เปอร์เซ็นต์กําไร = กำไร / ยอดขาย คูณด้วย 100% และ กำไร = ยอดขายรวม – ต้นทุนรวม
    profit = profit_total - cost_total
    try:
        percent_profit = (profit / cost_total) * 100
        return round_down(percent_profit, precision), round_down(profit, precision)
    except:
        return 0.0, 0.0


def round_down(num, digits):
    factor = 10.0 ** digits
    return math.floor(num * factor) / factor


def sell_stock(data, status, last_price, port, stop_loss):
    BOARD_LOT = 100
    volume = port['actual_volume']
    volume -= volume % BOARD_LOT
    cost = data['cost_total']
    symbol = data['symbol']

    if status:  # Check status market
        # Case (A)
        condition = data['condition']
        percent_retracement = condition.percent_retracement

        # Calucate low price
        if last_price > data['high_price']:
            high_price = last_price
            # semaphore.acquire()
            realtime = Realtime.objects.get(name=symbol)
            # semaphore.release()
            realtime.high_price = high_price
            realtime.save(update_fields=['high_price'])
        else:
            high_price = data['high_price']

        # Calculate percent of retracement
        percent, profit = calculate_profit(last_price, high_price)
        logger.debug("Retracement %.2f", percent)

        # Check retracement or stop loss
        if percent >= percent_retracement or stop_loss:
            if stop_loss:
                logger.debug("Stop Loss")
            else:
                logger.debug("Sell isuue : Cost = %.2f฿, Revenue = %.2f฿, Profit = %.2f฿",
                             cost, last_price * volume, (last_price * volume) - cost)

            # Send data to group
            # semaphore.acquire()
            realtime = Realtime.objects.get(name=symbol)
            # semaphore.release()
            realtime.status_order = 'Pending(S)'
            realtime.save(update_fields=['status_order'])
            current_portfolio(symbol)
            send_data_to_group(
                {'event': 'disable', 'data': '#button-sell'}, symbol)
            # trade_api.equity_place_order('SELL', symbol, volume)
            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)
            status = loop.run_until_complete(
                place_oder('SELL', symbol, volume))
            send_data_to_group(
                {'event': 'stop_trade', 'symbol': symbol, 'status': status}, symbol)  # Stop trade

        return percent
    else:
        logger.info(
            'Cannot place Market Price order when market is not open or quantity incorrect')


def check_market_status():
    current_time = datetime.datetime.now().time()
    if (current_time > time_open_i['start'] and current_time < time_open_i['end']):
        return 'Open(I)'
    elif (current_time > time_open_ii['start'] and current_time < time_open_ii['end']):
        return 'Open(II)'
    else:
        return 'Closed'
