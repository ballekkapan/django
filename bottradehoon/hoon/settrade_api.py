import os
from re import sub
from asgiref.sync import sync_to_async
from settrade.openapi import Investor
from pprint import pprint, pformat
from channels.layers import get_channel_layer
import asyncio
import math
import time
from settrade.openapi.mqtt import Subscriber
from .db import update_database_order
from threading import Thread, Lock
import threading
import queue
from .models import Realtime
from contextlib import suppress
import logging
from decouple import config

app_id = config('STT-OPENAPI-AUTH-APP-ID')
app_secret = config('STT-OPENAPI-AUTH-APP-SECRET')
app_code = config('STT-OPENAPI-AUTH-APP-CODE')
broker_id = config('STT-OPENAPI-AUTH-BROKER-ID')
account_no = config('STT-OPENAPI-AUTH-ACCOUNT-NO')
account_pin = config('STT-OPENAPI-AUTH-ACCOUNT-PIN')
is_auto_queue = False

stock_data = {
    'data': '',
    'event': '',
}

investor = Investor(
    app_id=app_id,
    app_secret=app_secret,
    app_code=app_code,
    broker_id=broker_id,
    is_auto_queue=is_auto_queue)

equity = investor.Equity(account_no=account_no)
lock = Lock()
port = {}
realtime_trade = {}
# 'NoneType' object is not subscriptable

# Create instance
logger = logging.getLogger('hoon')

# **********************************************************
# *  Description: Subscribe Equity Order When Update       *
# *                                                        *
# *  Date:          Version:    Programmer:       Reason:  *
# *  2021-10-14      1.0        Ekkapan C.        created  *
# **********************************************************


def subscribe_order(result, subscriber):
    # pprint(result)
    orders = []
    status = result['data']['status']

    # add new key
    result['data']['vol'] = result['data']['volume']
    result['data']['matched'] = result['data']['matched_volume']
    result['data']['balance'] = result['data']['balance_volume']

    stock_data['event'] = 'history'
    stock_data['function'] = 'subscribe_order'
    orders.append(result['data'])
    stock_data['data'] = orders
    logger.debug('Enqueue status value is %s', status)
    queue_status.put(result['data'])
    # Send data to group
    send_data_to_group(stock_data, result['data']['symbol'])
    if status == 'M':  # SX, M
        wait_status = realtimeStatus()
        wait_status.join()  # Wait Status finish
        # subscriber.stop()


queue_status = queue.Queue(10)  # This will add items 10 to the queue.
lock = Lock()
realtime = investor.RealtimeDataConnection()
sub = realtime.subscribe_equity_order(
    account_no, on_message=subscribe_order)
sub.start()


class trade():
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def session_check_or_login(self, name):
        account_info = equity.get_account_info()
        status = account_info['success']
        status_code = account_info['status_code']
        if status and status_code == 200:
            # logger.debug("%s | Connected", name)
            return
        else:
            logger.debug("status_code = %s, status = %s",
                         status_code, status)

            for i in range(2):
                # Login
                try:
                    Investor(
                        app_id=app_id,
                        app_secret=app_secret,
                        app_code=app_code,
                        broker_id=broker_id,
                        is_auto_queue=is_auto_queue)
                except Exception as e:
                    logger.debug(e)

                sesion_reset = equity.get_account_info()
                time.sleep(0.5)
                if sesion_reset['status_code'] != 200:
                    continue
                logger.debug("Restart session successfuly")

    @sync_to_async
    def get_info(self, symbol):
        self.session_check_or_login('get_info')
        account_info = equity.get_account_info()
        stock_data['event'] = 'account_info'
        stock_data['data'] = account_info['data']
        send_data_to_group(stock_data, symbol)
        return stock_data

    def portfolio(self, symbol):
        self.session_check_or_login('portfolio')
        portfolio = equity.get_portfolio()  # list_data
        data = portfolio['data']
        status = portfolio['success']

        if status and data:
            for port in data:
                if port['symbol'] == symbol:
                    port['status'] = True
                    return port

        return {'status': False, 'actual_volume': 0}

    @sync_to_async
    def list_order(self, symbol):
        self.session_check_or_login("list_order")
        orderlist = equity.get_orders()
        # pprint(orderlist)
        orders = []
        data = {}

        stock_data['event'] = 'history'
        stock_data['function'] = 'list_order'

        stock_data['data'] = orderlist['data']
        send_data_to_group(stock_data, symbol)
        return stock_data

    # **********************************************************
    # *  Description: Settrade API (consumers).                *
    # *  Description: Send data stock to websocket             *
    # *                                                        *
    # *  Date:          Version:    Programmer:       Reason:  *
    # *  2021-10-14      1.0        Ekkapan C.        created  *
    # **********************************************************
    @sync_to_async
    def equity_place_order(self, side, symbol, volume):
        # callback function executed when equity order is updated
        self.session_check_or_login("equity_place_order")
        # Subscribe order
        orders = []
        volume = math.floor(volume)
        # Side (BUY,SELL)
        place_order = equity.place_order(
            symbol=symbol,
            price=0,
            volume=volume,
            side=side,
            price_type="MARKET",
            # price_type="SPECIAL_MARKET",
            pin=account_pin),

        logger.debug('\n' + pformat(place_order, indent=2))

        place_order = place_order[0]
        status_code = place_order["status_code"]

        if place_order["success"] == True and status_code == 200:
            # send_data_to_group(stock_data)        
            return True

        else:
            with suppress(Exception):
                realtime = Realtime.objects.get(name=symbol)
                realtime.status_order = 'M'
                realtime.save(update_fields=['status_order'])
            return False

    # **********************************************************
    # *  Description: Check status order and then save to      *
    # *               database                                 *
    # *                                                        *
    # *  Date:          Version:    Programmer:       Reason:  *
    # *  2021-11-09      1.0        Ekkapan C.        created  *
    # **********************************************************

    def order_status(self, order_no):
        self.session_check_or_login("order_status")
        # Data send to group

        order = equity.get_order(order_no=order_no)

        status_code = order['status_code']
        data = order['data']

        if status_code == 200:
            return True, data
        else:
            logger.debug(order['message'])
            return False, data

    # **********************************************************
    # *  Description: Cancel order                             *
    # *                                                        *
    # *  Date:          Version:    Programmer:       Reason:  *
    # *  2021-11-09      1.0        Ekkapan C.        created  *
    # **********************************************************
    def cancel_order(self, order_no):
        self.session_check_or_login("cancel_order")
        cancel_order = equity.cancel_order(order_no=order_no, pin=account_pin)
        return cancel_order


class realtimeStatus(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.daemon = True
        self.start()
        self.__stop_event = threading.Event()
        stock_data['event'] = 'reset_condition'

    def stop(self):
        self.__stop_event.set()

    def run(self):
        lock.acquire()
        while not queue_status.empty():
            data = queue_status.get()
            if data['status'] == 'M':
                symbol = data['symbol']
                curent_port = trade().portfolio(symbol)
                update_database_order(data, curent_port, realtime_trade)
                lock.release()
                break


def current_portfolio(symbol):
    global port
    port = trade().portfolio(symbol)
    return port


def current_realtime_trade(data):
    global realtime_trade
    realtime_trade = data['data']
    return data


# **********************************************************
# *  Description: Send data stock to websocket             *
# *                                                        *
# *  Date:          Version:    Programmer:       Reason:  *
# *  2021-10-14      1.0        Ekkapan C.        created  *
# **********************************************************W
def send_data_to_group(data, group_name):
    # Send data to group
    # room_group_name = 'equity'
    channel_layer = get_channel_layer()
    loop = asyncio.new_event_loop()

    asyncio.set_event_loop(loop)

    loop.run_until_complete(channel_layer.group_send(group_name, {
        'type': 'send_equity_update',
        'message': data,
    }))
    loop.close()
