# chat/consumers.py
import json
from os import terminal_size
from .settrade_api import trade, send_data_to_group, current_portfolio
from channels.generic.websocket import AsyncWebsocketConsumer
from asgiref.sync import sync_to_async
from django_celery_beat.models import PeriodicTask, IntervalSchedule
from settrade.openapi import Investor
from .tasks import realtimePrice, realtimeAutoTrade, semaphore
from pprint import pprint
from .db import *
from contextlib import suppress
import logging
from django.shortcuts import redirect
from decouple import config

# Create logger
# FORMAT = '%(asctime) - %(levelname) | %(name)s | %(message)'
logger = logging.getLogger('hoon')
continue_trade = config('CONTINUE_TRADE')
# define Equity Variable
is_order_done = False
# Object Data
stock_data = {
    'data': '',
    'event': '',
}


class EquityConsumer(AsyncWebsocketConsumer):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.last_price = {}
        self.symbol = ''
        self.flag_status = True
        self.order_no = ''

        # Create Instance
        self.equity_api = trade()

    # **********************************************************
    # *  Description: Send data stock to websocket             *
    # *                                                        *
    # *  Date:          Version:    Programmer:       Reason:  *
    # *  2021-10-14      1.0        Ekkapan C.        created  *
    # **********************************************************

    @ sync_to_async
    def get_condition_stock(self, symbol):
        stock_data['event'] = 'get_condition'
        stock_data['data'] = load_condition(symbol)
        send_data_to_group(stock_data, symbol)

    # **********************************************************
    # *  Description: Send data stock to websocket             *
    # *                                                        *
    # *  Date:          Version:    Programmer:       Reason:  *
    # *  2021-10-14      1.0        Ekkapan C.        created  *
    # **********************************************************
    @ sync_to_async
    def reset_condition_stock(self):
        stock_data['event'] = 'reset_condition'
        send_data_to_group(stock_data, self.room_name)

    # **********************************************************
    # *  Description: Create Celery Beat                       *
    # *                                                        *
    # *  Date:          Version:    Programmer:       Reason:  *
    # *  2021-10-14      1.0        Ekkapan C.        created  *
    # **********************************************************
    @ sync_to_async
    def add_celery_beat(self, autotrade):
        task = PeriodicTask.objects.filter(name="every-3-seconds")
        if len(task) > 0:
            task = task.first()
            task.args = json.dumps([autotrade])
            task.save()
        else:
            schedule, created = IntervalSchedule.objects.get_or_create(
                every=3, period=IntervalSchedule.SECONDS)
            task = PeriodicTask.objects.create(
                interval=schedule, name='every-3-seconds', task="hoon.tasks.realtime_equity", args=json.dumps([autotrade]))

    # **********************************************************
    # *  Description: Start interval realtime price            *
    # *                                                        *
    # *  Date:          Version:    Programmer:       Reason:  *
    # *  2021-10-14      1.0        Ekkapan C.        created  *
    # **********************************************************
    @ sync_to_async
    def realtime_price_start(self, symbol):
        # Change Global Symbol
        with suppress(Exception):
            self.realtime_price.stop()

        self.realtime_price = realtimePrice(symbol)
        self.realtime_price.start()

    @sync_to_async
    def stop_auto_trade(self):
        with suppress(Exception):
            self.realtime.stop()

        stock_data['event'] = 'reset_condition'
        send_data_to_group(stock_data, self.room_name)

    @sync_to_async
    def enable_button(self, id):
        send_data_to_group(
            {'event': 'enable', 'data': id}, self.room_name)

    @sync_to_async
    def disable_button(self, id):
        send_data_to_group(
            {'event': 'disable', 'data': id}, self.room_name)

    @sync_to_async
    def start_trading(self, symbol):
        queryset = Realtime.objects.get(name=symbol)
        trading_status = queryset.trading_status
        if trading_status:
            # start realtimeAutoTrade
            self.realtime_price_start(symbol)
            self.get_condition_stock(symbol)

            # start realtimeAutoTrade
            self.realtime = realtimeAutoTrade(symbol)
            self.realtime.start()

            send_data_to_group({'event': 'start_trade'}, symbol)

            self.enable_button('#button-buy')
            self.enable_button('#button-sell')

    # **********************************************************
    # *  Description: Equity consumers                         *
    # *                                                        *
    # *  Date:          Version:    Programmer:       Reason:  *
    # *  2021-10-14      1.0        Ekkapan C.        created  *
    # **********************************************************
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'stock_%s' % self.room_name

        logger.debug("group name is '%s', room name is '%s'",
                     self.room_group_name, self.room_name)

        if self.room_name:
            await self.accept()

            # Join room group
            await self.channel_layer.group_add(
                self.room_name,
                self.channel_name
            )
            # await self.disable_button('.btn')
            # list order
            await self.equity_api.list_order(self.room_name)

            await self.get_condition_stock(self.room_name)
            # Get Account Info
            await self.equity_api.get_info(self.room_name)

            # Real time price
            await self.realtime_price_start(self.room_name)

            # Check Database
            count = await filter_database(self.room_name)
            if count == 0:
                logger.debug(
                    'Create reltimes databse object({}), Please reset condition when last price update'.format(self.room_name))
                await init_realtime_trade(self.room_name, self.last_price, 1)

            await self.enable_button('#button-reset')
            await self.enable_button('#button-trade')

            # Check Trading Status
            await self.start_trading(self.room_name)

    # **********************************************************
    # *  Description: Disconnect Websocket                     *
    # *                                                        *
    # *  Date:          Version:    Programmer:       Reason:  *
    # *  2021-10-14      1.0        Ekkapan C.        created  *
    # **********************************************************

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )
        await self.stop_auto_trade()
        config('STT-OPENAPI-AUTH-APP-ID')
        with suppress(Exception):
            self.realtime_price.stop()
            logger.info("Stop Realtime Price & Trade")
        redirect('/logout')
        logger.debug('redirect logout page')

    # **********************************************************
    # *  Description: Receive message from WebSocket           *
    # *                                                        *
    # *  Date:          Version:    Programmer:       Reason:  *
    # *  2021-10-14      1.0        Ekkapan C.        created  *
    # **********************************************************

    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        type = text_data_json['type']
        message = text_data_json['message']
        pprint(message)
        logger.info("Websocket receive event : %s", type)
        # Place order case
        if type == 'place_order':
            symbol = message['symbol']
            side = message['side']

            if side == 'BUY':
                await self.disable_button('#button-sell')
                await self.equity_api.equity_place_order(
                    side, symbol, int(message['volume']))
            else:  # Sell all actual volume
                await self.disable_button('#button-buy')
                port = current_portfolio(symbol)
                status = await self.equity_api.equity_place_order(
                    side, symbol, port['actual_volume'])

                if not continue_trade and status:
                    await self.stop_auto_trade()
                if status:
                    with suppress(Exception):
                        symbol = message['symbol']
                        await init_realtime_trade(symbol, self.last_price, 1)

        elif type == 'autotrade':
            state = text_data_json['state']
            symbol = message['symbol']
            await filter_database(symbol)
            await clear_status(symbol)
            if state:
                if symbol != 'SYM':
                    logger.debug('เริ่มต้นการเทรด')
                    # start realtimeAutoTrade
                    await self.realtime_price_start(symbol)
                    await self.get_condition_stock(symbol)

                    # start realtimeAutoTrade
                    self.realtime = realtimeAutoTrade(symbol)
                    self.realtime.start()
                    await self.enable_button('#button-buy')
                    await self.enable_button('#button-sell')

            else:
                logger.debug('ยกเลิกการเทรด')
                with suppress(Exception):
                    self.realtime.stop()
                await self.disable_button('#button-buy')
                await self.disable_button('#button-sell')
                await self.enable_button('#button-trade')

            await set_trade_status(symbol, state)

        elif type == 'reset_condition':
            await self.stop_auto_trade()
            with suppress(Exception):
                symbol = message['symbol']
                await init_realtime_trade(symbol, self.last_price, 1)
                logger.debug('reset conditions')
            await self.enable_button('#button-reset')

    # **********************************************************
    # *  Description: Receive message from room group          *
    # *                                                        *
    # *  Date:          Version:    Programmer:       Reason:  *
    # *  2021-10-14      1.0        Ekkapan C.        created  *
    # **********************************************************

    async def send_equity_update(self, event):
        message = event['message']

        if message['event'] == 'realtime_stock':
            data = message['data']
            symbol = data['symbol']
            self.last_price[symbol] = data['last']
            self.symbol = data['symbol']

        elif message['event'] == 'stop_trade':
            status = message['status']
            logger.debug(status)
            if not continue_trade and status:
                await self.stop_auto_trade()
            if status:
                with suppress(Exception):
                    symbol = message['symbol']
                    await init_realtime_trade(symbol, self.last_price, 1)
            else:
                await self.stop_auto_trade()

        elif message['event'] == 'realtime_profit':
            await self.enable_button('#button-trade')

        elif message['event'] == 'history':
            if message['function'] != 'list_order':
                await self.enable_button('#button-buy')
                await self.enable_button('#button-sell')

        # Send message to WebSocket
        await self.send(text_data=json.dumps({'message': message}))
