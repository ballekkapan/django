// Declare Variable
let conditons = {
  amount: 0,
  quantity: 0,
  percent_drop: [],
  multiply: [],
  percent_profit: [],
  drop_pullback: 0,
  percent_retracement: 0,
  stop_loss: 0,
};

let realtimes_buy = {
  volume: 0,
  times: 0,
  multiply: 0,
  percent_buy: 0,
  drop_pullback: 0,
  "%profit": 0,
  "%drop_pullback": 0,
  estimate_amount: 0,
};

let realtimes_sell = {
  volume: 0,
  profit_total: 0,
  retracement: 0,
  "%profit": 0,
};

//**********************************************************
//*  Description: Open websocket (consumers).              *
//*                                                        *
//*  Date:          Version:    Programmer:       Reason:  *
//*  2021-10-04      1.0        Ekkapan C.        created  *
//**********************************************************
// let socket = new WebSocket("ws://localhost:8000/ws/equity/");
const roomName = JSON.parse(document.getElementById("room-name").textContent);
let queryString = window.location.search;
queryString = queryString.substring(1);
console.log(queryString);
let socket = new WebSocket(
  "ws://" + window.location.host + "/ws/equity/" + roomName + "/"
);
console.log(socket);
let red_color = "#fa6565";
let green_color = "#02b156";
let yellow_color = "#FBDB46";

socket.onmessage = function (event) {
  let data = JSON.parse(event.data);
  let event_case = data.message["event"];
  let json_data = data.message["data"];
  console.log("event : " + event_case);
  switch (event_case) {
    case "enable":
      document.querySelectorAll(json_data).forEach(function (btn) {
        btn.disabled = false;
      });
      break;

    case "disable":
      document.querySelectorAll(json_data).forEach(function (btn) {
        btn.disabled = true;
      });
      break;

    case "start_trade":
      const btn_trade = document.getElementById("button-trade");
      btn_trade.classList.remove("btn-yellow");
      btn_trade.classList.add("btn-gray", "active");
      btn_trade.innerText = "หยุดชั่วคราว";
      state = true;
      break;

    case "disable_state":
      state = document.querySelectorAll(json_data).disabled;
      socket.send(
        JSON.stringify({
          message: { side: "BUY", symbol: symbol, volume: volume },
          type: "place_order",
        })
      );
      break;

    case "realtime_stock":
      let main_data = ["symbol", "time"];
      console.log("Realtime", json_data);
      main_data.forEach(function (data, index) {
        format = [json_data[data], `ข้อมูลล่าสุด: ${json_data[data]}`];
        document.getElementById("main-" + data).innerHTML = format[index];
      });
      //
      array_data = [
        data.message["data"]["last"],
        data.message["data"]["percent"],
      ];
      array_data.forEach(updatePrice);
      break;

    case "get_condition":
      console.log("Condition", json_data);
      let data_condition = [
        "amount",
        "quantity",
        "percent_drop",
        "multiply",
        "percent_profit",
        "drop_pullback",
        "percent_retracement",
        "stop_loss",
      ];
      data_condition.forEach(function (data) {
        conditons[data] = json_data[data];
      });
      updateConditions();
      break;

    case "reset_condition":
      // console.log("reset_condition");
      btn = document.getElementById("button-trade");
      // ยกเลิกการเทรด reset active
      btn.classList.remove("btn-gray", "active");
      btn.classList.add("btn-yellow");
      btn.innerText = "เทรด";
      state = false;
      break;

    case "account_info":
      const account_info = [
        "credit_limit",
        "line_available",
        "cash_balance",
        "can_buy",
      ];
      account_info.forEach(function (data) {
        if (data == "can_buy" || data == "can_sell") {
          document.getElementById("account." + data).innerHTML =
            json_data[data] + " / " + json_data["can_sell"];
        } else {
          document.getElementById("account." + data).innerHTML =
            json_data[data].toLocaleString("en-US") + "<span>THB</span>";
        }
      });
      break;

    case "realtime_profit":
      const sell_data = json_data["SELL"];
      const buy_data = json_data["BUY"];
      const status = json_data["status"];
      console.log("Realtime Profit", json_data);

      Object.keys(realtimes_buy).forEach(function (key) {
        if (buy_data[key]) {
          realtimes_buy[key] = buy_data[key];
        } else {
          realtimes_buy[key] = "-";
        }
      });

      Object.keys(realtimes_sell).forEach(function (key) {
        realtimes_sell[key] = sell_data[key];
      });

      document.getElementById(
        "buy.status"
      ).innerHTML = `${realtimes_buy["volume"]} / ${realtimes_buy["estimate_amount"]}`;
      document.getElementById(
        "sell.status"
      ).innerHTML = `${sell_data["volume"]} / ${sell_data["profit_total"]}`;
      document.getElementById("account.status").innerHTML = status;

      updateConditions();

      // Update price Condition while trading
      document.getElementById("main-init_price").innerHTML =
        sell_data["init_price"].toFixed(2);
      if (buy_data["low_price"]) {
        document.getElementById("main-low_price").innerHTML =
          buy_data["low_price"].toFixed(2);
      }
      document.getElementById("main-high_price").innerHTML =
        sell_data["high_price"].toFixed(2);
      break;

    case "history":
      console.log("History", data.message["data"]);
      const data_history = data.message["data"];
      data_history.forEach(function (data) {
        updateTable(data);
      });
      break;
  }
};

//**********************************************************
//*  Description: Event form place order                   *
//*                                                        *
//*  Date:          Version:    Programmer:       Reason:  *
//*  2021-10-04      1.0        Ekkapan C.        created  *
//**********************************************************
let btns = document.querySelectorAll(".btn");
btns.forEach(function (btn) {
  btn.addEventListener("click", function (e) {
    this.disabled = true;
  });
});

document.getElementById("button-buy").onclick = function (e) {
  console.log("Buy");
  symbol = document.getElementById("main-symbol").innerText;
  volume = document.getElementById("buy.status").innerText;
  const volumeArray = volume.split(" /");
  console.log(volumeArray);
  socket.send(
    JSON.stringify({
      message: { side: "BUY", symbol: symbol, volume: volumeArray[0] },
      type: "place_order",
    })
  );
};
document.getElementById("button-sell").onclick = function (e) {
  console.log("Sell");
  symbol = document.getElementById("main-symbol").innerText;
  volume = document.getElementById("sell.status").innerText;
  const volumeArray = volume.split(" /");
  console.log(volumeArray);
  socket.send(
    JSON.stringify({
      message: { side: "SELL", symbol: symbol, volume: volumeArray[0] },
      type: "place_order",
    })
  );
};
document.getElementById("button-trade").onclick = function (button) {
  symbol = document.getElementById("main-symbol");
  class_name = this.getAttribute("class");
  class_name = class_name.match(/ active/m);

  let state;
  // this.class_name = this.class_name.replace(" active", "");
  if (class_name == null) {
    // เริ่มต้นการเทรด set active
    this.classList.remove("btn-yellow");
    this.classList.add("btn-gray", "active");
    this.innerText = "หยุดชั่วคราว";
    state = true;
  } else {
    // ยกเลิกการเทรด reset active
    this.classList.remove("btn-gray", "active");
    this.classList.add("btn-yellow");
    this.innerText = "เทรด";
    state = false;
  }
  socket.send(
    JSON.stringify({
      message: { symbol: symbol.innerText },
      type: "autotrade",
      state: state,
    })
  );
};
document.getElementById("button-reset").onclick = function (e) {
  if (confirm("Do you want to reset the initial condition?")) {
    symbol = document.getElementById("main-symbol");
    socket.send(
      JSON.stringify({
        message: { symbol: symbol.innerText },
        type: "reset_condition",
      })
    );
  } else {
    this.disabled = false;
  }
};

//**********************************************************
//*  Description: Select Equity                            *
//*                                                        *
//*  Date:          Version:    Programmer:       Reason:  *
//*  2021-10-04      1.0        Ekkapan C.        created  *
//**********************************************************
function getEquity(selectObject) {
  let symbol = selectObject.value;
  window.location.replace(`/equity/${symbol}`);
}

//**********************************************************
//*  Description: Update History Table                     *
//*                                                        *
//*  Date:          Version:    Programmer:       Reason:  *
//*  2021-10-04      1.0        Ekkapan C.        created  *
//**********************************************************
function updatePrice(item, index) {
  arQuery = ["#main-last", "#main-percent"];
  percent = item;
  percent_id = document.querySelector(arQuery[index]);

  // percent_id.classList.remove(text_class[0]);
  // void percent_id.offsetWidth;
  if (index == 1) {
    if (percent > 0) {
      // percent_id.classList.add("blink-green");
      percent_id.innerText = `Change (+${percent}%)`;
    } else if (percent < 0) {
      // percent_id.classList.add("blink-red");
      percent_id.innerText = `Change (${percent}%)`;
    } else {
      // percent_id.classList.add("blink-gray");
      percent_id.innerText = `Change (${percent}%)`;
    }
  } else {
    text_color = percent_id.getAttribute("class");
    text_class = text_color.match(/blink-[a-z]+/m);
    percent_id.classList.add("blink-green");
    percent_id.innerHTML = `${percent} <span>THB</span>`;
  }
}

function set_color_number(id, number) {
  if (number < 0) {
    id.style.color = red_color;
  } else if (number == 0) {
    id.style.color = yellow_color;
  } else {
    id.style.color = green_color;
  }
}

function updateConditions() {
  console.log("Update Condition", conditons);
  Object.keys(conditons).forEach(function (key) {
    con_num = conditons[key];
    id = document.getElementById("condition." + key);
    switch (key) {
      case "amount":
        id.innerHTML = `${con_num.toLocaleString("en-US")} <span>THB</span>`;
        break;
      case "quantity":
        id.innerHTML = `${realtimes_buy["times"]} / ${con_num} <span>ครั้ง</span>`;
        break;
      case "percent_drop": // ช้อนซื้อ
        text = con_num.join(", ");
        id.childNodes[0].nodeValue = `${realtimes_buy["%profit"]}`;
        id.children[1].setAttribute("data-bs-original-title", text);
        break;
      case "multiply": // ตัวคูณช้อนซื้อ
        text = con_num.join(", ");
        id.childNodes[0].nodeValue = `${realtimes_buy["multiply"]}`;
        id.children[1].setAttribute("data-bs-original-title", text);
        break;
      case "drop_pullback": // (A)
        text = con_num.join(", ");
        id.childNodes[0].nodeValue = `${realtimes_buy["%drop_pullback"]}`;
        id.children[1].setAttribute("data-bs-original-title", text);
        break;
      case "percent_profit": // (C)
        let profit = realtimes_sell["%profit"];
        id.childNodes[0].nodeValue = `${profit}`;
        profit <= 0
          ? id.classList.add("color-danger")
          : id.classList.add("color-success");
        id.children.innerHTML = `/${con_num}<span>%</span>`;
        break;
      case "percent_retracement": // (B)
        id.childNodes[0].nodeValue = con_num;
        break;
      case "stop_loss": // (B)
        id.childNodes[0].nodeValue = con_num;
        break;
    }
  });
}
