"""
ASGI config for bottradehoon project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/asgi/
"""

from django.core.asgi import get_asgi_application
import os
import django
from channels.http import AsgiHandler
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack
from hoon.rounting import websocket_urlpatterns

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'bottradehoon.settings')

# django.setup()
django_asgi_app = get_asgi_application()


application = ProtocolTypeRouter({
    # "http": AsgiHandler(),
    "http": django_asgi_app,
    # Just HTTP for now. (We can add other protocols later.)
    "websocket": AuthMiddlewareStack(
        URLRouter(
            websocket_urlpatterns
        )
    ),
})
