from django.db import models
from django.conf import settings

# Create your models here.


class Condition(models.Model):
    name = models.CharField(max_length=200, primary_key=True)  # Name of symbol
    symbol = models.CharField(max_length=10)  # Symbol of Equity
    amount = models.FloatField(default=10000)  # จำนวนครั้งแรกในการ
    quantity = models.IntegerField(default=4)  # จำนวนครั้งในการช้อนซื้อ
    # ตัวคูณอ้างอิงจำนวนครั้งในการช้อนซื้อ, ตัวคูณอ้างอิง % ที่ระบบเข้าซื้อเมื่อราคาลดลง
    # gain = models.FloatField(default=1)
    # (C) % กำไรเมื่อหุ้นเพิ่มขึ้น, จากนั้นขายหุ้น Full Position TP Ratio
    percent_profit = models.FloatField(default=1.1)
    # (B) % ราคาเมื่อหุ้นเพิ่มขึ้นแล้วลดลง, จากนั้นขายหุ้น Full Position TP Retracement
    percent_retracement = models.FloatField(default=0.1)
    # (A) % ราคาเมื่อหุ้นลดลงแล้วดีดกลับ, จากนั้นเข้าช้อนซื้อหุ้น
    stop_loss = models.FloatField(default=-1.1)

    # Configure การช้อนซื้อ
    def cover_default():
        return {
            "percent": [1, 3, 4, 4, 4, 7, 8],
            "multiply": [2, 4, 8, 16, 32, 64, 128],
            "drop_pullback": [0.25, 0.25, 0.25, 0.3, 0.3, 0.4, 0.5]
        }

    cover_conf = models.JSONField(
        "CoverConf", default=cover_default)  # percent ในการช้อนซื้อ

    def __str__(self):
        return self.symbol


class Realtime(models.Model):
    name = models.CharField(max_length=100, primary_key=True)
    symbol = models.CharField(max_length=10)  # Symbol of Equity
    condition = models.ForeignKey(Condition, on_delete=models.CASCADE)
    # amount = models.FloatField()  # จำนวนครั้งแรกในการ
    # equity_price = models.FloatField()  # ราคาหุ้น
    init_price = models.FloatField(default=0)  # จำนวนครั้งแรกในการ
    high_price = models.FloatField(default=0)  # จำนวนครั้งแรกในการ
    low_price = models.FloatField(default=0)  # จำนวนครั้งแรกในการ
    times = models.IntegerField(default=0)  # ครั้งที่ช้อนซื้อ
    status_order = models.CharField(max_length=100)
    last_order_no = models.CharField(max_length=100)
    cost_total = models.FloatField(default=0)
    stock_total = models.FloatField(default=0)
    trading_status = models.BooleanField(default=False)

    class Meta:
        ordering = ('name', )
        verbose_name = "Realtimes"
        verbose_name_plural = "Realtimes"
        # db_table = 'Autotrade'

    def __str__(self):
        return self.name


class Result(models.Model):
    # author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, default="admin")
    name = models.CharField(max_length=50, primary_key=True)
    created_at = models.DateField(auto_now_add=True, null=True)
    # published_at = models.DateTimeField(blank=True, null=True)
    symbol = models.CharField(max_length=5)  # Symbol of Equity
    time = models.IntegerField(default=0)
    side = models.CharField(max_length=5)  # Symbol of Equity
    realize_profit = models.FloatField(default=0)
    # percent_profit = models.FloatField(default=0)
    json_data = models.JSONField()

    def __str__(self):
        return self.name
