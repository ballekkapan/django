from __future__ import absolute_import, unicode_literals
import os

from celery import Celery, app
from celery.app import task
from django.conf import settings
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'bottradehoon.settings')

app = Celery('bottradehoon')
app.conf.enable_utc = False
app.conf.update(timezone='Asia/Bangkok')

app.config_from_object(settings, namespace='CELERY')

app.conf.beat_schedule = {
    # 'every-10-seconds': {
    #     'task': 'hoon.tasks.realtime_equity',
    #     'schedule': 10,
    #     'args': ('send data from celery to channel layer',)
    # },
}

app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print(f'Request: {self.request!r}')
