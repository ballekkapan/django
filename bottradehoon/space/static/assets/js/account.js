// Initial
getAccount();
getOrders();

// Event onclick portfolio button
document.getElementById("btn-home").onclick = function () {
  getAccount();
  getOrders();
};

// get account
function getAccount() {
  let url = "/api/account";
  fetch(url)
    .then((res) => res.json())
    .then((account_data) => {
      console.log("Account", account_data);
      json_data = account_data["data"];
      const account_info = [
        "credit_limit",
        "line_available",
        "cash_balance",
        "can_buy",
      ];
      account_info.forEach(function (data) {
        if (data == "can_buy" || data == "can_sell") {
          document.getElementById("account." + data).innerHTML =
            json_data[data] + " / " + json_data["can_sell"];
        } else {
          document.getElementById("account." + data).innerHTML =
            json_data[data].toLocaleString("en-US") + "<span>THB</span>";
        }
      });
    });
}

// get list orders
function getOrders() {
  let url = "/api/get_orders";
  fetch(url)
    .then((res) => res.json())
    .then((data) => {
      console.log("Orders", data);
      data.forEach(function (data) {
        updateTable(data);
      });
    });
}

//**********************************************************
//*  Description: Update History Table                     *
//*                                                        *
//*  Date:          Version:    Programmer:       Reason:  *
//*  2021-10-04      1.0        Ekkapan C.        created  *
//**********************************************************
function updateTable(data) {
  // Get Tabel
  const table = document.getElementById("table-history");
  const length = table.rows.length;
  const col_num = 8;
  //   let time = data["entry_time"].match(/blink-[a-z]+/m);
  let rowCount = table.rows.length;
  text_node = [
    rowCount, // No.
    // data["order_no"],
    data["symbol"],
    data["side"],
    data["entry_time"],
    data["vol"],
    data["matched"],
    data["balance"],
    data["status"],
  ];

  // console.log(data["order_no"]);
  for (let i = 0; i < length; i++) {
    row = table.rows[i];
    id = row.getAttribute("id");
    // console.log(id, " == ", data["order_no"]);
    if (id == data["order_no"]) {
      // Update data
      for (let j = 0; j < col_num; j++) {
        if (j == 0) {
        } else if (j != 3) {
          row.cells[j].innerHTML = text_node[j];
        }
      }
      class_name = row.getAttribute("class");
      class_name = class_name.match(/history-[a-z]+/m);

      row.classList.remove(class_name[0]);
      void row.offsetWidth;
      row.classList.add(data["side"] == "BUY" ? "history-buy" : "history-sell");
      return;
    }
  }

  // Insert New Order
  let newRow = table.insertRow(0);
  newRow.setAttribute("id", data["order_no"]);
  newRow.setAttribute("class", "history-a");

  // Append a text node to the cell
  for (let i = 0; i < col_num; i++) {
    // add cell
    let newCell = newRow.insertCell(i);
    let newText = document.createTextNode(text_node[i]);
    if (i == 0) {
      newText = document.createTextNode(text_node[i].toString());
      newCell.style.left = "0px";
      newCell.className = "sticky text-center";
      newCell.appendChild(newText);
    } else if (i == 1) {
      newCell.style.left = "46px";
      newCell.className = "sticky-left";
      newCell.appendChild(newText);
    } else if (i == 3) {
      newCell.appendChild(newText);
    } else if (i == 2) {
      const item = document.createElement("div");
      const span_item = document.createElement("span");
      span_item.className = "d-block text-white text-center";
      span_item.style.padding = "5px 5px 5px 5px";
      span_item.style.borderRadius = "3px";
      span_item.style.backgroundColor =
        data["side"] == "BUY" ? "#02b156" : "#fa6565";
      span_item.innerText = data["side"];
      item.appendChild(span_item);
      newCell.appendChild(item);
    } else if (i == 7) {
      newCell.style.right = "0px";
      newCell.className = "sticky-right";
      const status = document.createElement("div");
      const i_status = document.createElement("i");
      const span_status = document.createElement("span");
      status.className = "status";
      i_status.className = "icon-dot-status yellow";
      span_status.appendChild(newText);
      i_status.appendChild(span_status);
      status.appendChild(i_status);
      newCell.appendChild(status);
      return;
    } else {
      // Volume Matched Banlace
      newText = document.createTextNode(text_node[i].toLocaleString("en-US"));
      newCell.appendChild(newText);
    }
  }
}
