from dataclasses import field
import imp
from pyexpat import model
from rest_framework import serializers
from .models import Realtime, Condition, Result

class RealtimeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Realtime
        fields = ['name']

class ConditionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Condition
        fields = ['name']

class ResultSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Result
        fields = ['name']