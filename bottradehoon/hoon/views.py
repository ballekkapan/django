from django.shortcuts import render, redirect
from pprint import pprint
from .models import Condition
from pprint import pprint
from django.contrib import messages
import logging
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from .settrade_api import equity as eq
from .settrade_api import trade
import subprocess
from pathlib import Path

# Create your views here.
# Create instance
logger = logging.getLogger('hoon')
cwd = Path(__file__).resolve().parent.parent.parent
version = subprocess.check_output(['git', 'describe'], cwd=cwd).strip()
version = version.decode("utf-8") 


@login_required
def index(request):
    # Query Data From Model
    data = Condition.objects.all()
    return render(request, 'equity.html', {'posts': data, 'room_name': 'home'})


@login_required
def portfolio(request):
    trade().session_check_or_login("portfolio")
    portfolio = eq.get_portfolio()
    logger.info(portfolio)
    return render(request, 'portfolio.html', {'version': version, 'posts': portfolio['data']})


@login_required
def equity(request, room_name):
    logger.debug(room_name)
    # Query Data From Model
    data = Condition.objects.all()
    return render(request, 'equity.html', {'version': version, 'posts': data, 'room_name': room_name})


def logout_view(request):
    logout(request)
    return redirect('/')
    

def login_view(request):
    # print(request.GET)
    next = request.GET.get('next')
    return render(request, 'login.html', {'next': next})


def login_user(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    next = request.GET.get('next', '')
    if user is not None:
        # Log a user in
        login(request, user)
        # messages.success(request, 'Your password was updated successfully!')
    else:
        # return HttpResponse("Incorrect user<br><a href=""/"">Get back to home page</a>")
        messages.warning(request, 'The username or password is incorrect!')
    return redirect(next)

# https://stackoverflow.com/questions/16750464/django-redirect-after-login-not-working-next-not-posting
