from asyncio import Task
from django.core.paginator import Paginator
from django.shortcuts import render, redirect
from pprint import pprint
from django.contrib import messages
import logging
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
import subprocess
from pathlib import Path
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import api_view
import datetime
from hoon.models import Condition, Result
from hoon.settrade_api import equity as eq
from hoon.settrade_api import trade
from hoon.serializers import ConditionSerializer

# Create your views here.
# Create instance
logger = logging.getLogger('hoon')
cwd = Path(__file__).resolve().parent.parent.parent
version = subprocess.check_output(['git', 'describe'], cwd=cwd).strip()
version = version.decode("utf-8")
per_page = 20


@login_required
def index(request):
    # Query Data From Model
    data = Condition.objects.all()
    # order_status = trade().order_status('GP25BL6BT')
    # pprint(order_status)
    return render(request, 'stock_space/index.html', {'posts': data, 'room_name': 'home'})


@login_required
def portfolio(request):
    trade().session_check_or_login("portfolio")
    portfolio = eq.get_portfolio()
    logger.info(portfolio)
    return render(request, 'stock_space/index.html', {'version': version, 'posts': portfolio['data']})


@login_required
def equity(request, room_name):
    logger.debug(room_name)
    # Query Data From Model
    data = Condition.objects.all()
    return render(request, 'stock_space/index.html', {'version': version, 'posts': data, 'room_name': room_name})


def logout_view(request):
    logout(request)
    return redirect('/stock_space/index.html')


def login_page(request):
    return render(request, 'stock_space/login.html')


def login_form(request):
    # print(request.GET)
    next = request.GET.get('next')
    return render(request, 'stock_space/login.html', {'next': next})


def login_user(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    next = request.GET.get('next', '')
    if user is not None:
        # Log a user in
        login(request, user)
        # messages.success(request, 'Your password was updated successfully!')
    else:
        # return HttpResponse("Incorrect user<br><a href=""/"">Get back to home page</a>")
        messages.warning(request, 'The username or password is incorrect!')
    return redirect(next)


def get_profit():
    today = datetime.date.today()
    list_monthly = []
    list_daily = []
    daily_sum_profit = []
    dialy_sum_loss = []
    total = {}

    symbols = Condition.objects.only('symbol')

    res = Result.objects.all()

    try:
        first_year = res[0].created_at.year
    except IndexError:
        return
    except AttributeError:
        first_year = today.year

    for y in range(today.year, int(first_year)-1, -1):
        # Months
        for m in range(1,13):

            month_profit = 0
            month_loss = 0
            name_y = datetime.datetime(y, m, 1)
            name_y = name_y.strftime("%b %y")

            # Calculate 31 Days
            for d in range(1, 32):
                # Calculate 1 Day
                day_profit = 0
                day_loss = 0

                days = Result.objects.filter(created_at__day=d,
                                             created_at__month=m,
                                             created_at__year=y,)

                if days:
                    # Sum 1 day
                    for sym in symbols:
                        day = days.filter(symbol=sym)
                        if day:
                            realize = day[len(
                                day)-1].json_data['portfolio']['realize_profit']
                            if realize > 0:
                                # Profit
                                day_profit += realize
                            else:
                                day_loss += realize

                    name_d = datetime.datetime(y, m, d)
                    name_d = name_d.strftime("%d %b %y")

                    daily_sum_profit.append(day_profit)
                    dialy_sum_loss.append(day_loss)
                    list_daily.append({name_d: [day_profit, day_loss]})

                # Sum 1 Month
                month_profit += day_profit
                month_loss += day_loss

            # Calculate 1 Mounth
            if month_profit !=0 and month_loss !=0:
                list_monthly.append({name_y: [month_profit, month_loss]})

    page_monthly = Paginator(list_monthly, per_page)
    page_daily = Paginator(list_daily, per_page)

    total_profit = sum([v for v in daily_sum_profit])
    total_loss = sum([v for v in dialy_sum_loss])

    total['profit'] = total_profit
    total['loss'] = total_loss
    total['nett'] = total_profit + total_loss

    return total, page_daily, page_monthly


class ConditionViewSet(viewsets.ModelViewSet):
    queryset = Condition.objects.all()
    serializer_class = ConditionSerializer


@api_view(['GET'])
def api_list(request):
    url = request.build_absolute_uri()
    api_urls = {
        'Portfolio': url + '/portfolio'
    }
    return Response(api_urls)


@api_view(['GET'])
def api_portfolio(request):
    trade().session_check_or_login("portfolio")
    portfolio = eq.get_portfolio()
    return Response(portfolio['data'])


@api_view(['GET'])
def api_account(request):
    trade().session_check_or_login("account")
    account = eq.get_account_info()
    return Response(account)


@api_view(['GET'])
def api_list_order(request):
    trade().session_check_or_login("list_order")
    orders = eq.get_orders()
    return Response(orders['data'])


@api_view(['GET'])
def api_profit(request):
    page_obj_daily = {}
    page_obj_monthly = {}
    page_number = request.GET.get('page')
    total, page_daily, page_monthly = get_profit()

    if int(page_number) <= page_daily.num_pages:
        for p in page_daily.page(page_number).object_list:
            key = list(p)
            page_obj_daily[key[0]] = list(p.values())[0]

    if int(page_number) <= page_monthly.num_pages:
        for p in page_monthly.page(page_number).object_list:
            key = list(p)
            page_obj_monthly[key[0]] = list(p.values())[0]

    page_obj = {'daily': {'data': page_obj_daily},
                'monthly': {'data': page_obj_monthly}, 'total': total, 'per_page': per_page}

    page_obj['daily']['num_pages'] = page_daily.num_pages
    page_obj['daily']['count'] = page_daily.count
    page_obj['monthly']['num_pages'] = page_monthly.num_pages
    page_obj['monthly']['count'] = page_monthly.count

    return Response(page_obj)

# https://stackoverflow.com/questions/16750464/django-redirect-after-login-not-working-next-not-posting
