from .base import *

DEBUG = True
ALLOWED_HOSTS = ['127.0.0.1']


# python manage.py runserver --settings=bottradehoon.settings.development
# python manage.py migrate --settings=bottradehoon.settings.development