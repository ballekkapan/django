from django.urls import path, re_path, include
from .views import *
from hoon.views import logout_view, login_user
from django.contrib.auth import views as auth_views

app_name = 'space'

urlpatterns = [
    path('', index, name='main'),
    path('equity/<str:room_name>/', equity, name='trade'),
    # path('portfolio', portfolio, name='portfolio'),
    path('logout', logout_view, name='logout'),
    path('login_form', login_form, name='login_form'),
    re_path(r'login', login_user),

    path('api', api_list, name='api-list'),
    path('api/portfolio', api_portfolio, name='api-portfolio'),
    path('api/account', api_account, name='api-account'),
    path('api/get_orders', api_list_order, name='api-get-orders'),
    re_path(r'api/get_profit/(?:page-(?P<page_number>\d+)/)?$', api_profit, name='api-get-profit'),
    path('api/condition/<str:symbol>', api_list_order, name='api-get-orders'),
]
