from os import name
from django.urls import path, re_path
from .views import *
from django.contrib.auth import views as auth_views

app_name = 'hoon'
urlpatterns = [
    path('', index),
    path('equity/<str:room_name>/', equity, name='equity'),
    path('portfolio', portfolio),
    path('logout', logout_view, name='logout'),
    path('login_form', login_view, name='login_view'),
    # re_path(r'login', login_user),
]
