//**********************************************************
//*  Description: Open websocket (consumers).              *
//*                                                        *
//*  Date:          Version:    Programmer:       Reason:  *
//*  2021-10-04      1.0        Ekkapan C.        created  *
//**********************************************************
// let socket = new WebSocket("ws://localhost:8000/ws/equity/");
const roomName = JSON.parse(document.getElementById("room-name").textContent);
let queryString = window.location.search;
queryString = queryString.substring(1);
// console.log(queryString);
let socket = new WebSocket(
  "ws://" + window.location.host + ":8001" + "/ws/equity/" + roomName + "/"
);
// console.log(socket);
let red_color = "#fa6565";
let green_color = "#02b156";
let yellow_color = "#FBDB46";

socket.onmessage = function (event) {
  let data = JSON.parse(event.data);
  let event_case = data.message["event"];
  let json_data = data.message["data"];
  console.log("event : " + event_case);
  switch (event_case) {
    case "enable":
      document.querySelectorAll(json_data).forEach(function (btn) {
        btn.disabled = false;
      });
      break;

    case "disable":
      document.querySelectorAll(json_data).forEach(function (btn) {
        btn.disabled = true;
      });
      break;

    case "disable_state":
      state = document.querySelectorAll(json_data).disabled;
      socket.send(
        JSON.stringify({
          message: { side: "BUY", symbol: symbol, volume: volume },
          type: "place_order",
        })
      );
      break;

    case "realtime_stock":
      let main_data = ["symbol", "time", "status"];
      main_data.forEach(function (data) {
        document.getElementById("main-" + data).innerHTML = json_data[data];
      });
      array_data = [
        data.message["data"]["last"],
        data.message["data"]["change"],
        data.message["data"]["percent"],
      ];
      array_data.forEach(updatePrice);
      break;

    case "get_condition":
      let data_condition = [
        "amount",
        "quantity",
        "percent_drop",
        "multiply",
        "percent_profit",
        "drop_pullback",
        "percent_retracement",
      ];
      data_condition.forEach(function (data) {
        document.getElementById("condition." + data).innerHTML =
          json_data[data].toLocaleString("en-US");
      });
      break;

    case "reset_condition":
      // console.log("reset_condition");
      btn = document.getElementById("button-trade");
      // ยกเลิกการเทรด reset active
      btn.classList.remove("btn-gray", "active");
      btn.classList.add("btn-yellow");
      btn.innerText = "เริ่มต้นเทรดต่อ";
      state = false;

      break;

    case "account_info":
      const account_info = [
        "credit_limit",
        "line_available",
        "cash_balance",
        "can_buy",
        "can_sell",
      ];
      account_info.forEach(function (data) {
        document.getElementById("account." + data).innerHTML =
          json_data[data].toLocaleString("en-US");
      });
      break;

    case "realtime_profit":
      // console.log(JSON.stringify(json_data, null, 4));
      const sell_data = json_data["SELL"];
      const buy_data = json_data["BUY"];
      const status = json_data["status"];
      const name_sell = ["volume", "profit_total", "retracement", "%profit"];
      const name_buy = [
        "volume",
        "times",
        "multiply",
        "percent_buy",
        "drop_pullback",
        "%profit",
        "%drop_pullback",
        "estimate_amount",
      ];
      document.getElementById("account.status").innerHTML = status;
      name_sell.forEach(function (data) {
        let name_id = "sell." + data;
        let id = document.getElementById(name_id);
        id.innerHTML = Number.isInteger(sell_data[data])
          ? sell_data[data]
          : sell_data[data].toFixed(2);
        set_color_number(id, sell_data[data]);
      });

      name_buy.forEach(function (data) {
        let name_id = "buy." + data;
        let id = document.getElementById(name_id);
        id.innerHTML = Number.isInteger(buy_data[data])
          ? buy_data[data]
          : buy_data[data].toFixed(2);
        set_color_number(id, buy_data[data]);
      });

      // Update price Condition while trading
      document.getElementById("main-init_price").innerHTML =
        sell_data["init_price"].toFixed(2);
      document.getElementById("main-low_price").innerHTML =
        buy_data["low_price"].toFixed(2);
      document.getElementById("main-high_price").innerHTML =
        sell_data["high_price"].toFixed(2);
      break;

    case "history":
      const data_history = data.message["data"];
      data_history.forEach(function (data) {
        updateTable(data);
      });
      break;
  }
};

//**********************************************************
//*  Description: Event form place order                   *
//*                                                        *
//*  Date:          Version:    Programmer:       Reason:  *
//*  2021-10-04      1.0        Ekkapan C.        created  *
//**********************************************************
let btns = document.querySelectorAll(".btn");
btns.forEach(function (btn) {
  btn.addEventListener("click", function (e) {
    this.disabled = true;
  });
});

document.getElementById("toggle-mode").onclick = function () {
  document.body.classList.toggle("light-mode");
  // console.log("Change Theme");
};

document.getElementById("button-buy").onclick = function (e) {
  // console.log("Buy")
  symbol = document.getElementById("main-symbol").innerText;
  volume = document.getElementById("buy.volume").innerText;
  socket.send(
    JSON.stringify({
      message: { side: "BUY", symbol: symbol, volume: volume },
      type: "place_order",
    })
  );
};
document.getElementById("button-sell").onclick = function (e) {
  // console.log("Sell")
  symbol = document.getElementById("main-symbol").innerText;
  volume = document.getElementById("sell.volume").innerText;
  socket.send(
    JSON.stringify({
      message: { side: "SELL", symbol: symbol, volume: volume },
      type: "place_order",
    })
  );
};
document.getElementById("button-trade").onclick = function (button) {
  symbol = document.getElementById("main-symbol");
  class_name = this.getAttribute("class");
  class_name = class_name.match(/ active/m);

  let state;
  // this.class_name = this.class_name.replace(" active", "");
  if (class_name == null) {
    // เริ่มต้นการเทรด set active
    this.classList.remove("btn-yellow");
    this.classList.add("btn-gray", "active");
    this.innerText = "หยุดชั่วคราว";
    state = true;
  } else {
    // ยกเลิกการเทรด reset active
    this.classList.remove("btn-gray", "active");
    this.classList.add("btn-yellow");
    this.innerText = "เริ่มต้นเทรดต่อ";
    state = false;
  }
  socket.send(
    JSON.stringify({
      message: { symbol: symbol.innerText },
      type: "autotrade",
      state: state,
    })
  );
};
document.getElementById("button-reset").onclick = function (e) {
  if (confirm("Do you want to reset the initial condition?")) {
    symbol = document.getElementById("main-symbol");
    socket.send(
      JSON.stringify({
        message: { symbol: symbol.innerText },
        type: "reset_condition",
      })
    );
  } else {
    this.disabled = false;
  }
};

//**********************************************************
//*  Description: Select Equity                            *
//*                                                        *
//*  Date:          Version:    Programmer:       Reason:  *
//*  2021-10-04      1.0        Ekkapan C.        created  *
//**********************************************************
const selects = document.querySelectorAll(".list-group-item");
// console.log(selects);
for (const select of selects) {
  select.addEventListener("click", function (event) {
    // select.className += " active";
    // console.log(select.className);
    socket.send(
      JSON.stringify({
        message: select.id,
        type: "select",
      })
    );
  });
}

//**********************************************************
//*  Description: Update History Table                     *
//*                                                        *
//*  Date:          Version:    Programmer:       Reason:  *
//*  2021-10-04      1.0        Ekkapan C.        created  *
//**********************************************************
function updatePrice(item, index) {
  arQuery = ["#main-last", "#main-change", "#main-percent"];
  arUnit = ["฿", "฿", "%"];

  percent = item;
  percent_id = document.querySelector(arQuery[index]);
  text_color = percent_id.getAttribute("class");
  text_class = text_color.match(/blink-[a-z]+/m);

  percent_id.classList.remove(text_class[0]);
  void percent_id.offsetWidth;
  // symbol.classList.add("blink-green");

  if (percent > 0) {
    percent_id.classList.add("blink-green");
    percent_id.innerText = "+" + percent + arUnit[index];
  } else if (percent < 0) {
    percent_id.classList.add("blink-red");
    percent_id.innerText = percent + arUnit[index];
  } else {
    percent_id.classList.add("blink-gray");
    percent_id.innerText = percent + arUnit[index];
  }
}

//**********************************************************
//*  Description: Update History Table                     *
//*                                                        *
//*  Date:          Version:    Programmer:       Reason:  *
//*  2021-10-04      1.0        Ekkapan C.        created  *
//**********************************************************
function updateTable(data) {
  // Get Tabel
  const table = document.getElementById("table-history");
  const length = table.rows.length;
  const col_num = 8;

  text_node = [
    data["order_no"],
    data["entry_time"],
    data["symbol"],
    data["side"],
    data["volume"],
    data["matched_volume"],
    data["balance_volume"],
    data["status"],
  ];

  col_width = ["10%", "20%", "10%", "15%", "10%", "10%", "10%", "10%"];

  // console.log(data["order_no"]);
  for (let i = 0; i < length; i++) {
    row = table.rows[i];
    id = row.getAttribute("id");

    // console.log(id, " == ", data["order_no"]);
    if (id == data["order_no"]) {
      // Update data
      for (let i = 0; i < col_num; i++) {
        if (i != 3) {
          row.cells[i].innerHTML = text_node[i];
        }
      }
      class_name = row.getAttribute("class");
      // console.log(class_name);
      class_name = class_name.match(/history-[a-z]+/m);

      row.classList.remove(class_name[0]);
      void row.offsetWidth;
      row.classList.add(data["side"] == "BUY" ? "history-buy" : "history-sell");
      return;
    }
  }

  // Insert New Order
  let newRow = table.insertRow(0);
  newRow.setAttribute("id", data["order_no"]);
  newRow.setAttribute("class", "history-a");

  // Append a text node to the cell
  for (let i = 0; i < col_num; i++) {
    let newCell = newRow.insertCell(i);
    let newText = document.createTextNode(text_node[i]);
    newCell.appendChild(newText);
    newCell.style.width = col_width[i];
  }
  newRow.cells[3].innerText = "";
  const item = document.createElement("div");
  item.className = "d-inline m-2 px-4 py-1 text-white rounded-2 align-middle";
  item.style.backgroundColor = data["side"] == "BUY" ? "#02b156" : "#fa6565";
  item.innerText = data["side"];
  newRow.cells[3].appendChild(item);
}

function set_color_number(id, number) {
  if (number < 0) {
    id.style.color = red_color;
  } else if (number == 0) {
    id.style.color = yellow_color;
  } else {
    id.style.color = green_color;
  }
}
